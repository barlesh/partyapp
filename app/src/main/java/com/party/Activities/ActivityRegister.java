package com.party.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.party.R;
import com.party.modules.Me;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

public class ActivityRegister extends AppCompatActivity {

    final private String TAG = "ActivityRegister";

    private String id, mail,gender , profileURL, coverURL, name, nickName ,ID, token, DOB, friends;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        token=intent.getStringExtra("token");
        name=intent.getStringExtra("name");
        gender=intent.getStringExtra("gender");
        DOB=intent.getStringExtra("birthday");
        friends=intent.getStringExtra("friends");
        Button IB1 = (Button) findViewById(R.id.button_confirm_registeration_detailes);
        IB1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // pop profile sending confirmation
                // the dialog will lunch the registration request to server
                // the registration requsest will receive answer from server
                // and accordigly start MainActivity or stay at this activity with error dialog massage
                popConfirmationDialog();
            }
        });

        ImageButton IB2 = (ImageButton) findViewById(R.id.button_change_profile_pic);
        IB2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // pick a new profile picture from phone's gallery
                changeProfilePic();
            }
        });
        Log.d(TAG, "ActivityRegister;onCreate; id is: " + ID);
    }


    void changeProfilePic(){
        Log.d(TAG, "changeProfilePic");
    }

    void popConfirmationDialog(){
        Log.d(TAG, "popConfirmationDialog");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("Are You Sure of the details?");

        // set dialog message
        alertDialogBuilder
                .setMessage("Click yes to Register!")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, run AsyncTask to register at Server
                        sendRegistrationRequest();

                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }


    void sendRegistrationRequest(){
        EditText editText=(EditText) findViewById(R.id.nickname);
        nickName=editText.toString();
        editText=(EditText) findViewById(R.id.mail_edit);
        mail=editText.toString();
        String url = "https://party-server.herokuapp.com/server_request/?" +GETRegistration();
        Log.d(TAG, "sendRegistrationRequest");
        AsyncServerRegisterReq registerReq= new AsyncServerRegisterReq(url);
        registerReq.execute();
    }


    void toMainActivity(){
        Intent intent = new Intent( ActivityRegister.this , MainActivity.class);
        Log.d(TAG, "toMainActivity");
        startActivity(intent);
        finish();
    }


    private class AsyncServerRegisterReq extends AsyncTask<Void, Void, String> {

        String strUrl, output;

        AsyncServerRegisterReq(String url) {
            strUrl = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(ActivityRegister.this, "onPreExecute", Toast.LENGTH_LONG).show();
        }

        @Override
        protected String doInBackground(Void... params) {

            //HttpHandler sh = new HttpHandler();
            HttpURLConnection urlConnection = null;
            // Making a request to url and getting response


            //String jsonStr = sh.makeServiceCall(url);

            try {
                // Construct the URL for the OpenWeatherMap query
                // Possible parameters are avaiable at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast
                URL url = new URL(strUrl);
                Log.d(TAG, "GET command to : " + strUrl);
                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                return getStringFromInputStream(inputStream);
                /*JSONObject reader = new JSONObject(readFully(inputStream));

                JSONObject request = reader.getJSONObject("request");

                String strRequest = request.getString("requset");


                JSONObject answer = reader.getJSONObject("answer");

                String strAnswer = request.getString("answer");*/

                // ans is not found. return "not found" string to postExecute
                /*if (strAnswer.compareTo("not found") == 0) {
                    return "not found";

                    //ans is found user id.... another object (Super user) sent
                } else if (strAnswer.compareTo("found") == 0) {
                    JSONObject superuser = reader.getJSONObject("superuser");

                    return jsonSuperUserToString(superuser);
                }
                */

            } catch (ProtocolException e) {
                Log.e(TAG, "Error:", e);

                // } catch (JSONException e) {
                //    Log.e(TAG, "Error:", e);
            } catch (IOException e) {
                Log.e(TAG, "Error:", e);
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "onPostExecute, string is:" + result );
            /*if(result.compareTo("Server error")==0){
                TextView textView= (TextView)findViewById(R.id.server_response);
                textView.setText(result);

            }else{
                Me me = new Me();

            }*/
            toMainActivity();
            return;
            /*finish();
            Log.d(TAG, "onPostExecute, after finish() wtf????" );
            // server returned user id not found. execute new registration activity
            if (result.compareTo("not found") == 0) {
                toRegisterActivity(result);
                // else, string is super user object
                // create super user singeltone using string
            } else {
                // create super user

                // if successfull, start main activity
                toMainActivity();

            }*/
        }



        /**
         * this function get json object and return it as string (for passing to post execute)
         *
         * @param jsonSU
         * @return
         */
        String jsonSuperUserToString(JSONObject jsonSU) {
            return "{\"user id\":94,\"name\":\"bar leshem\",\"friends list\":[13,57,78,34],\"num of friends\":450,\"party list\":[5367893,9247603,3847609]}";
        }

        public String readFully(InputStream entityResponse) throws IOException {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length = 0;
            while ((length = entityResponse.read(buffer)) != -1) {
                baos.write(buffer, 0, length);
            }
            return baos.toString();
        }
    }


    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
    String GETRegistration(){

        return "req=registration&ID=" + ID+ "&name=" + name +  "&nickname=" + nickName + "&mail=" + mail + "&gender="+gender + "&profilepicURL=http://graph.facebook.com/" + ID+"/picture?type=square&friends={\"arr\":[" + friends +"]}";
    }
}
