package com.party.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.drawable.Icon;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.party.Adapters.ControlMembersMenuAdapter;
import com.party.Adapters.DrawerAdapter;
import com.party.Adapters.DrawerItem;
import com.party.PartyControlFragments.ControlContentFragment;
import com.party.PartyControlFragments.ControlEquipFragment;
import com.party.PartyControlFragments.ControlMainFragment;
import com.party.PartyControlFragments.ControlMapFragment;
import com.party.PartyControlFragments.ControlMembersFragment;
import com.party.R;
import com.party.modules.User;

import java.util.ArrayList;
import java.util.List;

public class ActivityControlParty extends AppCompatActivity implements OnMapReadyCallback {

    final private int POSITION_MAP_ITEM = 0;
    final private int POSITION_EQUIP_ITEM = 1;
    final private int POSITION_MEMBER_ITEM = 2;
    final private int POSITION_CONTENT_ITEM = 3;
    private ActionBarDrawerToggle mDrawerToggle;
    private String[] mPlanetTitles = {"Map", "Equipnment", "Users"};
    private CharSequence mTitle, mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    static public FragmentManager fragmentManager;
    protected LocationManager locationManager;
    private DrawerAdapter adapter;
    List<DrawerItem> dataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_party);
        mTitle = mDrawerTitle = getTitle();
        dataList = new ArrayList<DrawerItem>();
        //mPlanetTitles = { "Map" , "Equipnment"};
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        dataList.add(new DrawerItem("Maps", R.drawable.maps_icon));
        dataList.add(new DrawerItem("Checklist", R.drawable.checklist_icon));
        dataList.add(new DrawerItem("Members", R.drawable.friends_icon));
        dataList.add(new DrawerItem("Content", R.drawable.content_icon));
        adapter = new DrawerAdapter(this, R.layout.drawer_list_item, dataList);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout.openDrawer(mDrawerList);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpen(View view) {
                getSupportActionBar().setTitle(mDrawerTitle);
                supportInvalidateOptionsMenu();
            }
        };

        Fragment fragment = new ControlMainFragment();
        show_frag(fragment);
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d("ActivityControlParty", "onItemClick. position is: " + position);
            selectItem(position);
        }
    }

    /** Swaps fragments in the main content view */
    private void selectItem(int position) {
        Fragment fragment =null;

        // Create a new fragment and specify the planet to show based on position
        switch(position){
            // map
            case POSITION_MAP_ITEM:

                Log.d("ActivityControlParty", "selectItem. position is: " + position);
                fragment = new ControlMapFragment();
                break;
            //equipent
            case POSITION_EQUIP_ITEM:
                fragment = new ControlEquipFragment();
                break;
            //members
            case POSITION_MEMBER_ITEM:
                showMemberDialog();
                return;
            //statuses
            case POSITION_CONTENT_ITEM:
                fragment = new ControlContentFragment();
                break;
        }
        show_frag(fragment);
        mDrawerList.setItemChecked(position, true);
    }


    private void showMemberDialog(){
        ArrayList<User> users = new ArrayList<User>();
        users.add(new User("Bar", "barlesh8@gmail.com", "nice", R.drawable.profile_bar, null));
        users.add(new User("Anat", "a@@gmail.com", "gr8", R.drawable.laughter_icon, null));
        final Dialog dialog = new Dialog(this);
        View view = getLayoutInflater().inflate(R.layout.member_dialog, null);
        ListView lv = (ListView) view.findViewById(R.id.member_dialog_list);
        ControlMembersMenuAdapter clad = new ControlMembersMenuAdapter(this, R.id.member_dialog_list, users);
        lv.setAdapter(clad);

        lv.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                // should pass fragment user details
                //String selectedFromList = (lv.getItemAtPosition(myItemInt).toString());
                Toast toast = Toast.makeText(getApplicationContext(), "Hello world!", Toast.LENGTH_LONG);
                toast.show();
                dialog.dismiss();
                Fragment fragment = new ControlMembersFragment();
                show_frag(fragment);

                mDrawerList.setItemChecked(POSITION_MEMBER_ITEM, true);
                //setTitle(mPlanetTitles[position]);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
        dialog.setContentView(view);
        dialog.show();

    }


    void show_frag(Fragment frag){
        Bundle args = new Bundle();
        args.putInt("some", 1);
        frag.setArguments(args);
        Log.d("ActivityControlParty", "FragmentManager");
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, frag).commit();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        try {
            map.setMyLocationEnabled(true);
        }catch(SecurityException e){

        }

        map.setTrafficEnabled(true);
        map.setIndoorEnabled(true);
        map.setBuildingsEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
    }
}
