package com.party.Activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.party.R;
import com.party.modules.Me;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class ActivityLogin2 extends AppCompatActivity {

    private static final String TAG = "ActivityLogin2";
    private TextView info;
    private LoginButton loginButton;
    private long long_id;
    private CallbackManager callbackManager;
    public LoginResult login;
    /**Duration of wait**/
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.layout);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    setContentView(R.layout.activity_login2);
                    try {
                        PackageInfo info = getPackageManager().getPackageInfo(
                                "com.party",
                                PackageManager.GET_SIGNATURES);
                        for (Signature signature : info.signatures) {
                            MessageDigest md = MessageDigest.getInstance("SHA");
                            md.update(signature.toByteArray());
                            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                        }
                    } catch (PackageManager.NameNotFoundException e) {

                    } catch (NoSuchAlgorithmException e) {

                    }
                    try1();

                    FacebookSdk.sdkInitialize(getApplicationContext());
                    callbackManager = CallbackManager.Factory.create();

                    setContentView(R.layout.activity_login2);

                    info = (TextView) findViewById(R.id.info);
                    loginButton = (LoginButton) findViewById(R.id.login_button);
                    loginButton.setReadPermissions(Arrays.asList("user_friends"));
                    loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            login=loginResult;
                            long_id  = Long.valueOf((String)loginResult.getAccessToken().getUserId()).longValue();
                            serverLoginSendUserId(long_id);
                            info.setText(
                                    "User ID: "
                                            + loginResult.getAccessToken().getUserId()
                                            + "\n" +
                                            "Auth Token: "
                                            + loginResult.getAccessToken().getToken()
                            );
                        }

                        @Override
                        public void onCancel() {
                            info.setText("Login attempt canceled.");
                        }

                        @Override
                        public void onError(FacebookException e) {
                            info.setText("Login attempt failed.");
                        }
                    });
                    if(loginButton.getText().toString().compareTo("Log out")==0)
                        serverLoginSendUserId(Long.valueOf(AccessToken.getCurrentAccessToken().getUserId()));

                        }
        },SPLASH_DISPLAY_LENGTH);
    }



    /**
     * this function sends JSON string to server with user id
     * the asynchronious requst from server will activate next event of app
     *
     * @param id
     * @return
     */
    void serverLoginSendUserId(long id) {


        // create url to be sent to server
        String Url = "https://party-server.herokuapp.com/server_request/?" +GETLogin(id);

        // send url to server. the async task will get ans, evaluate it & run next app event
        serverRequset(Url);


    }


    void serverRequset(String urlReq) {
        AsyncServerLoginReq loginReq = new AsyncServerLoginReq(urlReq);
        loginReq.execute();

    }

    String GETLogin(long id) {
        return "req=user_login&ID=" + id;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    void try1() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.party",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }


    void toRegisterActivity(final long id){
        final Intent intent = new Intent( ActivityLogin2.this , ActivityRegister.class);
        GraphRequestBatch batch = new GraphRequestBatch(
                GraphRequest.newMeRequest(
                        AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject jsonObject,
                                    GraphResponse response) {
                                try{
                                    intent.putExtra("id", jsonObject.getString("id"));
                                    intent.putExtra("birthday",jsonObject.getString("birthday"));
                                    intent.putExtra("gender",jsonObject.getString("gender"));
                                    intent.putExtra("name",jsonObject.getString("first_name")+jsonObject.getString("last_name"));
                                }catch(Exception e){

                                }
                            }
                        }),
                GraphRequest.newMyFriendsRequest(
                        AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONArrayCallback() {
                            @Override
                            public void onCompleted(
                                    JSONArray jsonArray,
                                    GraphResponse response) {
                                    try{

                                        String friends=jsonArray.getJSONObject(0).getString("id");
                                        for(int i=1;i<jsonArray.length();i++)
                                            friends+=","+jsonArray.getJSONObject(i).getString("id");
                                        intent.putExtra("friends",friends);
                                    }catch(Exception e){

                                    }


                            }
                        })
        );
        batch.addCallback(new GraphRequestBatch.Callback() {
            @Override
            public void onBatchCompleted(GraphRequestBatch graphRequests) {

                Log.d(TAG, "toRegisterActivity");
                startActivity(intent);
                finish();
            }
        });
        batch.executeAsync();

    }

    void toMainActivity(){
        Intent intent = new Intent( ActivityLogin2.this , MainActivity.class);
        Log.d(TAG, "toMainActivity");
        startActivity(intent);
        finish();
    }

    private class AsyncServerLoginReq extends AsyncTask<Void, Void, String> {

        String strUrl;
        String token;
        AsyncServerLoginReq(String url) {
            strUrl = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(ActivityLogin2.this, "Json Data is downloading", Toast.LENGTH_LONG).show();

        }

        @Override
        protected String doInBackground(Void... params) {

            //HttpHandler sh = new HttpHandler();
            HttpURLConnection urlConnection = null;
            // Making a request to url and getting response


            //String jsonStr = sh.makeServiceCall(url);

            try {
                // Construct the URL for the OpenWeatherMap query
                // Possible parameters are avaiable at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast
                URL url = new URL(strUrl);
                Log.d(TAG, "GET command to : " + strUrl);
                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                return getStringFromInputStream(inputStream);
                /*JSONObject reader = new JSONObject(readFully(inputStream));

                JSONObject request = reader.getJSONObject("request");

                String strRequest = request.getString("requset");


                JSONObject answer = reader.getJSONObject("answer");

                String strAnswer = request.getString("answer");*/

                // ans is not found. return "not found" string to postExecute
                /*if (strAnswer.compareTo("not found") == 0) {
                    return "not found";

                    //ans is found user id.... another object (Super user) sent
                } else if (strAnswer.compareTo("found") == 0) {
                    JSONObject superuser = reader.getJSONObject("superuser");

                    return jsonSuperUserToString(superuser);
                }
                */

            } catch (ProtocolException e) {
                Log.e(TAG, "Error:", e);

           // } catch (JSONException e) {
            //    Log.e(TAG, "Error:", e);
            } catch (IOException e) {
                Log.e(TAG, "Error:", e);
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "onPostExecute, string is:" + result );
            // server returned user id not found. execute new registration activity
            if (result.compareTo("Not found")==0) {
                toRegisterActivity(long_id);
                // else, string is super user object
                // create super user singeltone using string
            } else {

            }
            return;
        }



        /**
         * this function get json object and return it as string (for passing to post execute)
         *
         * @param jsonSU
         * @return
         */
        String jsonSuperUserToString(JSONObject jsonSU) {
            return "{\"user id\":94,\"name\":\"bar leshem\",\"friends list\":[13,57,78,34],\"num of friends\":450,\"party list\":[5367893,9247603,3847609]}";
        }

        public String readFully(InputStream entityResponse) throws IOException {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length = 0;
            while ((length = entityResponse.read(buffer)) != -1) {
                baos.write(buffer, 0, length);
            }
            return baos.toString();
        }
    }

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }


}