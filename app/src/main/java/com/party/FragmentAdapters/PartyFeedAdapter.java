package com.party.FragmentAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.R;
import com.party.modules.Party;

/**
 * Created by barlesh on 17/12/16.
 */


public class PartyFeedAdapter extends ArrayAdapter<Party> {

    Context context;
    int layoutResourceId;
    Party data[] = null;

    public PartyFeedAdapter(Context context, int layoutResourceId, Party[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PartyHolder holder = null;

        if(row == null)
        {
            if( layoutResourceId < 0  || parent == null){
                Log.d("PartyFeedAdapter", "layoutResourceId < 0  || parent = null");
            }
            try{
               // LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(layoutResourceId, parent, false);
            }catch (Exception e){
                Log.e("PartyFeedAdapter", "Error ", e);
                return row;
            }


            holder = new PartyHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.item_party_feed_icon);
            holder.partyName = (TextView)row.findViewById(R.id.item_party_feed_txt_name);
            holder.friendName = (TextView)row.findViewById(R.id.item_txt_friend_name);


            row.setTag(holder);
        }
        else
        {
            holder = (PartyHolder)row.getTag();
        }

        Party party = data[position];
        holder.partyName.setText(party.getName());
        holder.friendName.setText(party.getFriend());
        holder.imgIcon.setImageResource(party.getIcon());

        return row;
    }

    static class PartyHolder
    {
        ImageView imgIcon;
        TextView partyName;
        TextView friendName;
    }
}
