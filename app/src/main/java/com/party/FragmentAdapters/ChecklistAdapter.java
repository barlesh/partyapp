package com.party.FragmentAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.R;
import com.party.modules.EquipmentItem;
import com.party.modules.Party;

/**
 * Created by Mor Hazan on 1/8/2017.
 */

public class ChecklistAdapter extends ArrayAdapter<EquipmentItem>{

        Context context;
        int layoutResourceId;
        EquipmentItem data[] = null;

        public ChecklistAdapter(Context context, int layoutResourceId, EquipmentItem[] data) {
            super(context, layoutResourceId,data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ChecklistAdapter.EquipmentHolder holder = null;

        if(view == null)
        {
            if( layoutResourceId < 0  || parent == null){
                Log.d("PartyFeedAdapter", "layoutResourceId < 0  || parent = null");
            }
            try{
                // LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(layoutResourceId, parent, false);
            }catch (Exception e){
                Log.e("PartyFeedAdapter", "Error ", e);
                return view;
            }


            holder = new EquipmentHolder();
            holder.userIcon = (ImageView)view.findViewById(R.id.equipment_user);
            holder.itemName = (TextView)view.findViewById(R.id.equipment_text);
            holder.acquired = (CheckBox)view.findViewById(R.id.equipment_check);


            view.setTag(holder);
        }
        else
        {
            holder = (ChecklistAdapter.EquipmentHolder)view.getTag();
        }

        EquipmentItem item= data[position];
        holder.itemName.setText(item.getItem());
        holder.acquired.setChecked(false);
        holder.userIcon.setImageResource(item.getIcon());

        return view;
    }

        static class EquipmentHolder
        {
            ImageView userIcon;
            TextView itemName;
            CheckBox acquired;
        }
}
