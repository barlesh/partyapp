package com.party.MainFragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.party.Activities.ActivityShowFeedParty;
import com.party.FragmentAdapters.PartyFeedAdapter;
import com.party.R;
import com.party.modules.Party;


public class FeedPartyFragment extends ListFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_feed_party, container, false);
        viewSetToolBar();
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Party party_data[] = getPartyData();

        PartyFeedAdapter adapter = new PartyFeedAdapter( getActivity(),
                R.layout.item_party_feed, getPartyData());
        //PartyFeedAdapter adapter = ArrayAdapter.createFromResource(getActivity(),
         //       R.array.Planets, android.R.layout.simple_list_item_1);
        setListAdapter(adapter);
        //getListView().setOnItemClickListener(this);
    }

    void viewSetToolBar(){
        setHasOptionsMenu(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d("FeedParyFragmnet", "onOptionsItemSelected. id: " + id + ". item: " + item.getTitle() );
        switch (id){
            case R.id.toolbar_feed_edit_filters:
                Log.d("FeedParyFragmnet", "toolbar_feed_edit_filters" );
                popFeedFiltersMenu();
                //return true;
                break;
            case R.id.toolbar_feed_search:
                //return true;
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void popFeedFiltersMenu(){
        /*
     * Inflate the XML view. activity_main is in
     * res/layout/form_elements.xml
     */
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View formElementsView = inflater.inflate(R.layout.feed_filters_menu,
                null, false);

        // You have to list down your form elements
        final CheckBox FoF = (CheckBox) formElementsView
                .findViewById(R.id.feed_filter_fof);

        final RadioGroup partyTypeRadioGroup = (RadioGroup) formElementsView
                .findViewById(R.id.feed_filter_RadioGroup);

        final EditText distanceEditText = (EditText) formElementsView
                .findViewById(R.id.feed_filter_distance);

        // the alert dialog
        new AlertDialog.Builder(getActivity()).setView(formElementsView)
                .setTitle("Feed FIlters")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        String toastString = "";

                    /*
                     * Detecting whether the checkbox is checked or not.
                     */
                        if (FoF.isChecked()) {
                            toastString += "Happy is checked!\n";
                        } else {
                            toastString += "Happy IS NOT checked.\n";
                        }

                    /*
                     * Getting the value of selected RadioButton.
                     */
                        // get selected radio button from radioGroup
                        int selectedId = partyTypeRadioGroup
                                .getCheckedRadioButtonId();

                        // find the radiobutton by returned id
                        RadioButton selectedRadioButton = (RadioButton) formElementsView
                                .findViewById(selectedId);

                        toastString += "Selected radio button is: "
                                + selectedRadioButton.getText() + "!\n";

                    /*
                     * Getting the value of an EditText.
                     */
                        toastString += "Name is: " + distanceEditText.getText()
                                + "!\n";

                        Log.d("FeedParyFragmnet", "before toast");
                        Toast.makeText(getActivity(), toastString, Toast.LENGTH_SHORT).show();
                        //showToast(toastString);
                        Log.d("FeedParyFragmnet", "before dialog.cancel");
                        dialog.cancel();
                    }

                }).show();
    }


    /*@Override
    public void onListItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getActivity(), "Item: " + position, Toast.LENGTH_SHORT).show();
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_feed, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        toShowFeedParty();
    }

    public void toShowFeedParty(){
        // TODO: extract party's data from party_data[posision] and place into intent
        Intent intent = new Intent(getActivity(), ActivityShowFeedParty.class);
        startActivity(intent);
    }



    /**
     * tempurary.... only until real data would be produced
     * @return
     */
    public Party[] getPartyData(){
        Party data[] = new Party[]
                {
                        new Party("karachana bayyar" , "Anat", R.drawable.wood, "active"),
                        new Party("BBBBBBlok" , "Mor", R.drawable.club, "pre")
                };
        return data;
    }
}
