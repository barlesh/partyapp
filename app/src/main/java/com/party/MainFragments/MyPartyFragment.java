package com.party.MainFragments;


import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.party.Activities.ActivityAddNewParty;
import com.party.Activities.ActivityControlParty;
import com.party.FragmentAdapters.PartyMyAdapter;
import com.party.R;
import com.party.modules.Party;


public class MyPartyFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_party, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Party partyData[] = getPartyData();

        PartyMyAdapter adapter = new PartyMyAdapter( getActivity(),
                R.layout.item_party_my, getPartyData());
        setListAdapter(adapter);

    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);
        // TODO

        toPartyControl();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d("FeedParyFragmnet", "onOptionsItemSelected. id: " + id + ". item: " + item.getTitle() );
        switch (id){
            case R.id.toolbar_feed_create_new_party:
                Log.d("FeedParyFragmnet", "toolbar_feed_edit_filters" );
                toCreateNewParty();
                //return true;
                break;
            case R.id.toolbar_feed_search:
                //return true;
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_my_party, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void toCreateNewParty(){
        // TODO: extract party's data from party_data[posision] and place into intent
        Intent intent = new Intent(getActivity(), ActivityAddNewParty.class);
        startActivity(intent);
    }
    private void toPartyControl(){
        // TODO: extract party's data from party_data[posision] and place into intent
        Intent intent = new Intent(getActivity(), ActivityControlParty.class);
        startActivity(intent);
    }

    /**
     * tempurary.... only until real data would be produced
     * @return
     */
    public Party[] getPartyData(){
        Party data[] = new Party[]
                {
                        new Party("Ansterdam 2016!!! :) :)" , "Bar", R.drawable.wood, "active"),
                        new Party("New Year's @ the FORUUUUM" , "Bar", R.drawable.club, "pre")
                };
        return data;
    }

}
