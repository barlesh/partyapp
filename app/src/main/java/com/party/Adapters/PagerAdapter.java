package com.party.Adapters;

/**
 * Created by barlesh on 16/12/16.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;

//?????package com.truiton.designsupporttabs;

        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentManager;

import com.party.AddFragments.EquipmentAddFragment;
import com.party.AddFragments.GeneralAddFragment;
import com.party.AddFragments.LocationAddFragment;
import com.party.AddFragments.MembersAddFragment;
import com.party.MainFragments.FeedPartyFragment;
import com.party.MainFragments.MyPartyFragment;
import com.party.MainFragments.ProfileFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    FragmentManager fragmentManager;
    String context;
    public PagerAdapter(FragmentManager fm, int NumOfTabs, String from) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        context=from;
    }

    @Override
    public Fragment getItem(int position) {
    if(context.compareTo("AddActivity")==0)
        switch (position) {
            case 0:
                GeneralAddFragment tab1 = new GeneralAddFragment();

                return tab1;
            case 1:
                MembersAddFragment tab2 = new MembersAddFragment();
                return tab2;
            case 2:
                EquipmentAddFragment tab3 = new EquipmentAddFragment();
                return tab3;
            case 3:
                LocationAddFragment tab4 = new LocationAddFragment();
                return tab4;
            default:
                return null;
        }
        else switch (position) {
            case 0:
                MyPartyFragment tab1 = new MyPartyFragment();

                return tab1;
            case 1:
                FeedPartyFragment tab2 = new FeedPartyFragment();
                return tab2;
            case 2:
                ProfileFragment tab3 = new ProfileFragment();
                return tab3;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
