package com.party.Adapters;

/**
 * Created by barlesh on 25/12/16.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.modules.User;
import com.party.R;
import java.util.ArrayList;

/********************************************************************************
 * Costume Adapter. a class to create adapter instance to pass to list
 *******************************************************************************/

public class ControlMembersMenuAdapter extends ArrayAdapter<User> {

    Context context;
    int layoutResourceId;
    ArrayList<User> data = null;
    private LayoutInflater mInflater;


    public ControlMembersMenuAdapter(Context context, int layoutResourceId, ArrayList<User> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.mInflater = LayoutInflater.from(context);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {

            //item_list
            convertView = mInflater.inflate(R.layout.item_member, null);

            holder = new ViewHolder();

            //fill the views
            holder.name = (TextView) convertView.findViewById(R.id.item_member_name);
            holder.icon = (ImageView) convertView.findViewById(R.id.item_member_image);
            holder.status = (TextView) convertView.findViewById(R.id.item_member_status);

            convertView.setTag(holder);
        }
        else {
            // Get the ViewHolder back to get fast access to the TextView
            // and the ImageView.
            holder = (ViewHolder) convertView.getTag();//
        }

        int icon2 = 0;
        holder.name.setText(data.get(position).getName());
        holder.status.setText(data.get(position).getStatus());
        /*if(data.get(position).get_like().compareTo(L_DONTCARE) == 0 ){
            icon2 = R.drawable.imgdontcare;
        }else if(data.get(position).get_like().compareTo(L_LIKE) == 0 ){
            icon2 = R.drawable.imglike;
        }else if(data.get(position).get_like().compareTo(L_DISLIKE) == 0 ){
            icon2 = R.drawable.imgdislike;
        }else{
            icon2=0;
        }*/

        holder.icon.setImageResource(data.get(position).getIcon());

        //holder.icon.setImageResource(icon2);


        return convertView;
    }

    class ViewHolder {
        TextView name;
        TextView status;
        ImageView icon;

    }
}
