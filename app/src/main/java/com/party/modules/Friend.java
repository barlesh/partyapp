package com.party.modules;

import java.util.ArrayList;

/**
 * Created by barlesh on 17/12/16.
 */

public class Friend extends User {
    public Friend(String name, String email, String status, int icon, ArrayList<User> friendsList, String status1, ArrayList<Party> parties) {
        super(name, email, status, icon, friendsList);
        Status = status1;
        Parties = parties;
    }

    private String Status;
    private ArrayList<Party> Parties;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public ArrayList<Party> getParties() {
        return Parties;
    }

    public void setParties(ArrayList<Party> parties) {
        Parties = parties;
    }
}
