package com.party.modules;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.net.URL;

/**
 * Created by Mor Hazan on 1/12/2017.
 */
public class Me {

    public static Me getInstance() {
        return ourInstance;
    }
    private static Me ourInstance = new Me();
    private String name,nickName,status,mail,friends,gender,ID;
    private int numOfFriends;
    LatLng myLocation;


    public LatLng getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(LatLng myLocation) {
        this.myLocation = myLocation;
    }

    public static Me getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(Me ourInstance) {
        Me.ourInstance = ourInstance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getNumOfFriends() {
        return numOfFriends;
    }

    public void setNumOfFriends(int numOfFriends) {
        this.numOfFriends = numOfFriends;
    }

    public URL getProfile() {
        return profile;
    }

    public void setProfile(URL profile) {
        this.profile = profile;
    }

    public URL getCover() {
        return cover;
    }

    public void setCover(URL cover) {
        this.cover = cover;
    }

    private URL profile,cover;

    public Me() {

    }
}
