package com.party.modules;

import com.party.R;

import java.util.ArrayList;

/**
 * Created by Mor Hazan on 1/8/2017.
 */

public class EquipmentItem {
    private String description;
    private boolean checked=false;
    private int icon;


    public EquipmentItem(String text, int icon, boolean checked){
        this.description=text;
        if(icon!=0)
            this.icon=icon;
        else this.icon=R.drawable.friends_icon;
        if(checked)
            this.checked=true;
    }
    public String getItem() {
        return description;
    }
    public void setItem(String name) {
        description= name;
    }
    public int getIcon() {
        return icon;
    }
    public void setIcon(int icon) {
        this.icon = icon;
    }
    public boolean isChecked() {
        return checked;
    }
    public void setChecked(boolean in){
        if(in){
            this.checked=true;
            this.icon= R.drawable.checkbox_checked;
        }else{
            this.checked=false;
            this.icon=R.drawable.checkbox;
        }
    }


}
