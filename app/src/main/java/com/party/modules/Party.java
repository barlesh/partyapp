package com.party.modules;

import java.util.ArrayList;

/**
 * Created by barlesh on 17/12/16.
 */

public class Party {
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getFriend() {
        return Friend;
    }

    public void setFriend(String friend) {
        Friend = friend;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public ArrayList<User> getUsers() {
        return Users;
    }

    public void setUsers(ArrayList<User> users) {
        Users = users;
    }

    public ArrayList<String> getToDoList() {
        return ToDoList;
    }

    public void setToDoList(ArrayList<String> toDoList) {
        ToDoList = toDoList;
    }

    public ArrayList<String> getEquipmentList() {
        return EquipmentList;
    }

    public void setEquipmentList(ArrayList<String> equipmentList) {
        EquipmentList = equipmentList;
    }

    private String Name;
    private String Location;
    private int icon;

    private String Friend;
    private String Status;
    private User Admin;
    private ArrayList<User> Users;
    private ArrayList<String> ToDoList;
    private ArrayList<String> EquipmentList;



    public Party(String n, String f, int icon, String status) { Name = n; Friend = f; this.icon = icon; Status = status; }

}
