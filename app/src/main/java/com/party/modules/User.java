package com.party.modules;

import java.util.ArrayList;

/**
 * Created by barlesh on 17/12/16.
 */

public class User {
    // basic info variables
    private String Name;
    private String Email;
    private String Status;
    private int Icon;

    public User(String name, String email, String status, int icon, ArrayList<User> friendsList) {
        Name = name;
        Email = email;
        Status = status;
        Icon = icon;
        FriendsList = friendsList;
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getIcon() {
        return Icon;
    }

    public void setIcon(int icon) {
        this.Icon = icon;
    }


    public ArrayList<User> getFriendsList() {
        return FriendsList;
    }

    public void setFriendsList(ArrayList<User> friendsList) {
        FriendsList = friendsList;
    }

    private ArrayList<User> FriendsList;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
