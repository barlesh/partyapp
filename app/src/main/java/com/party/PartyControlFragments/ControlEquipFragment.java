

/**
 * Created by barlesh on 25/12/16.
 */
package com.party.PartyControlFragments;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.party.Activities.ActivityProfileEdit;
import com.party.FragmentAdapters.ChecklistAdapter;
import com.party.R;
import com.party.modules.EquipmentItem;
import com.party.modules.Party;


public class ControlEquipFragment extends ListFragment{
    ChecklistAdapter adapter;
    EquipmentItem data[];
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.control_equip_fragment, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        data = getEquipmentData();
        adapter = new ChecklistAdapter(getActivity(),R.layout.equipment_list_item,data);
        setListAdapter(adapter);


    }

    public void toProfileEdit(){
        Intent intent = new Intent(getActivity(), ActivityProfileEdit.class);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);

    }

    public EquipmentItem[] getEquipmentData(){
        EquipmentItem data[] = new EquipmentItem[]
                {
                        new EquipmentItem("Hammer",R.drawable.bar_marker,false),
                        new EquipmentItem("Generator",R.drawable.adi_marker,true)
                };
        return data;
    }
}

