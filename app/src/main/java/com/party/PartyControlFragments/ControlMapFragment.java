package com.party.PartyControlFragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.party.Activities.ActivityProfileEdit;
import com.party.R;


public class ControlMapFragment extends Fragment implements OnMapReadyCallback{
    MapView mapView;
    GoogleMap googleMap;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.control_map_fragment, container, false);
        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void toProfileEdit(){
        Intent intent = new Intent(getActivity(), ActivityProfileEdit.class);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume(){
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
        mapView.onLowMemory();
    }
    @Override
    public void onMapReady(GoogleMap map) {
        this.googleMap = map;
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(31.262562, 34.802105) , 14.0f) );
        map.addMarker(new MarkerOptions().position(new LatLng(31.263773, 34.800337)).title("Anat rockin'").icon(BitmapDescriptorFactory.fromResource(R.drawable.anat_marker)));
        map.addMarker(new MarkerOptions().position(new LatLng(31.262510, 34.802083)).title("Bar's wasted").icon(BitmapDescriptorFactory.fromResource(R.drawable.bar_marker)));
        map.addMarker(new MarkerOptions().position(new LatLng(31.262773, 34.802118)).title("Adi's at the bank").icon(BitmapDescriptorFactory.fromResource(R.drawable.adi_marker)));
    }
}
