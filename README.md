# Project_Mobile_Programming
Final project for Mobile Programming Course in Ben Gurion University
This android application is a social parties and hangouts control application, giving groups of users the ability to interact efficiently while going out. Main features include location show of your mates, party planner features and automatic detection of their current physical status and a "Panic button" feature that can make all the difference in dangerous situations.
